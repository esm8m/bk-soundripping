"""
Converts a single csf file to an equivalent .wav file
first argument: csf file to convert
second argument (optional): location to put file in (if directory) or
    name to give file (if includes a file)
    If second argument not given, the file will be given the name
    <original file>.wav and put in the directory of the script
    Note that if you want to give a directory or put it in a specific directory,
    it must already exist - this script will not make new ones.
"""

import pyximport
pyximport.install()

from adpcm import decode_block

import os
import os.path
import wave
import struct
import sys

if __name__ == "__main__":
    if (len(sys.argv) > 3 or len(sys.argv) < 2):
        print >>sys.stderr, "usage: %s <file to convert> <destination>" % sys.argv[0]
        sys.exit(1)

    file_ = sys.argv[1]
    dest = ''
    if len(sys.argv) == 3:
        if os.path.isfile(sys.argv[2]):
            dest = sys.argv[2]
        elif os.path.isdir(sys.argv[2]):
            dest = os.path.join(sys.argv[2],(os.path.split(file_)[1]+'.wav'))
        else:
            #doesn't exist - assume a file that needs to be made
            dest = sys.argv[2]
    else:
        dest = os.path.join('.',(os.path.split(file_)[1]+'.wav'))
    
    #this entire following section from raw_adpcm.py
    
    #settings for the file to dump into
    wfp = wave.open(dest, 'wb')
    #these are all assumptions based off of what seems to work from delroth's code
    #may need to be changed for certain files?
    wfp.setnchannels(1)
    wfp.setsampwidth(2)
    wfp.setframerate(22050)
    wfp.setnframes(200000) #this one in particular is arbitrary
    
    fp = open(file_,'rb')
    
    #based on offset used in csl-dump.py; may not be accurate for csf
    fp.seek(0xA0)
    start_off = 0xA0;
    
    coeffs = struct.unpack('>' + 'h' * 0x10, fp.read(0x20))
    
    start_off += 0x20
    decoded = decode_block(fp, start_off, 100000, 100000, coeffs, 0, 0)
    wfp.writeframesraw(''.join(decoded))
