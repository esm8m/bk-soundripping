#! /usr/bin/env python

"""
This rips lines that take place in scenes or in the field
first argument: directory with files from disc
second argument: directory to put converted voice files. These will be in the
    format of folders with the original file names, each containing several
    .wavs named after the internal file numbering, since each .csl file
    contains several individual lines.
Note that to get all the lines in the game, you need to run this twice
once for each disk
"""

import pyximport
pyximport.install()

from adpcm import decode_block

import os
import os.path
import wave
import struct
import sys

#support code from csl-dump.py
def get_uint32(csf, i):
    return struct.unpack('>I', ''.join(csf[i:i+4]))[0]

def get_song_id(csf):
    return get_uint32(csf, 0x28)
    
class CSL:
    def __init__(self):
        self.parts = []
        self.parts_off = []

    def parse(self, fp):
        fp.seek(0)
        magic = struct.unpack('4s', fp.read(4))[0]
        if magic != b'CSL ':
            raise ValueError("not a valid CSL file (bad magic)")

        nparts = struct.unpack('>I', fp.read(4))[0]
        for i in range(nparts):
            self.parts_off.append(struct.unpack('>I', fp.read(4))[0])

        fp.seek(0, 2)
        self.parts_off.append(fp.tell())

        for (part_start, part_end) in zip(self.parts_off, self.parts_off[1:]):
            fp.seek(part_start)
            self.parts.append(list(fp.read(part_end - part_start)))

        return self
              
#script for doing the ripping
if __name__ == '__main__':
    if len(sys.argv) != 3:
        print >>sys.stderr, "usage: %s <disc 1 or 2> <destination>" % sys.argv[0]
        sys.exit(1)
    
    origin = sys.argv[1]
    dest = sys.argv[2]

    #loop through the files from the disc   
    for file_ in os.listdir(origin):
        if not (file_.startswith('FILE_50004') or file_.startswith('FILE_3000') or file_.startswith('FILE_700042')):
            #not a story file, skip
            continue
        
        #this entire following section from csl-dump.py
        fp = open(os.path.join(origin,file_), 'rb')
        #put ripped files in folders for the CSL they're from
        os.makedirs(os.path.join(sys.argv[2],file_))
        outpath = os.path.join(sys.argv[2],file_)

        csl = CSL()
        csl.parse(fp)

        print "%d sounds" % len(csl.parts)
        for csf, off in zip(csl.parts, csl.parts_off):
            print "- %08X (at offset %08X)" % (get_song_id(csf), off)

            wfp = wave.open(os.path.join(outpath, "%08X.wav" % get_song_id(csf)),
                            'wb')
            wfp.setnchannels(1)
            wfp.setsampwidth(2)
            wfp.setframerate(22050)
            wfp.setnframes(1000000) # arbitrary... to fix?

            fp.seek(off + 0xA0)
            coeffs = struct.unpack('>hhhhhhhhhhhhhhhh', fp.read(0x20))
            decoded = decode_block(fp, off + 0xC0, 1000000, 1000000,
                                   coeffs, 0, 0)
            wfp.writeframesraw(''.join(decoded))
            
