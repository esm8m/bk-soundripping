DEF SAMPLES_PER_FRAME = 14

cdef short_to_string(unsigned short decoded):
    return '%c%c' % (decoded & 0xFF, decoded >> 8)

def decode_block(fp, unsigned int offset, unsigned int samples,
                 unsigned int size, coefs, int hist1, int hist2):
    cdef unsigned int sample_id, off
    cdef int coef1, coef2
    cdef int decoded
    cdef unsigned int hdr, scale, coef_idx, byte
    cdef int nibble

    fp.seek(offset)
    bytestring = map(ord, fp.read(size))

    all_decoded = []

    sample_id = 0
    off = 0
    try:
        for sample_id in xrange(samples):
            if sample_id % SAMPLES_PER_FRAME == 0:
                hdr = bytestring[off + sample_id / 2]
                scale = 1 << (hdr & 0xF)
                coef_idx = (hdr >> 4) & 0xF
                coef1 = coefs[coef_idx * 2]
                coef2 = coefs[coef_idx * 2 + 1]
                off += 1

            byte = bytestring[off + sample_id / 2]
            if sample_id % 2:
                nibble = byte & 0xF
            else:
                nibble = byte >> 4
            nibble = nibble - 16 if nibble >= 8 else nibble
            decoded = nibble * scale
            decoded <<= 11
            decoded += 1024 + coef1 * hist1 + coef2 * hist2
            decoded >>= 11
            if decoded < -2**15:
                decoded = -2**15
            elif decoded > 2**15 - 1:
                decoded = 2**15 - 1

            hist2 = hist1
            hist1 = decoded
            all_decoded.append(short_to_string(decoded))
    except:
        pass

    return all_decoded
