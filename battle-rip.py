#! /usr/bin/env python

"""
Used to rip just the voices played in battle
first argument: directory with files from disc
second argument: directory to put converted voice files, which will be named
    <original file>.wav.
Note that disc 1 and 2 have the same files in the same places for battle
(at least as far as I can tell)
"""

import pyximport
pyximport.install()

from adpcm import decode_block

import os
import os.path
import wave
import struct
import sys

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print >>sys.stderr, "usage: %s <disc 1 or 2> <destination>" % sys.argv[0]
        sys.exit(1)
    
    origin = sys.argv[1]
    dest = sys.argv[2]

    #loop through the files from the disc   
    for file_ in os.listdir(origin):
        if not (file_.startswith('FILE_700040') or file_.startswith('FILE_700041')):
            #not a battle file, skip
            continue
        
        #this entire following section from raw_adpcm.py
    
        #settings for the file to dump into
        wfp = wave.open(os.path.join(dest,(file_+".wav")), 'wb')
        #these are all assumptions based off of what seems to work from delroth's code
        #may need to be changed for certain files?
        wfp.setnchannels(1)
        wfp.setsampwidth(2)
        wfp.setframerate(22050)
        wfp.setnframes(200000) #this one in particular is arbitrary
    
        fp = open(os.path.join(origin,file_),'rb')
    
        #based on offset used in csl-dump.py; may not be accurate for csf
        fp.seek(0xA0)
        start_off = 0xA0;
    
        coeffs = struct.unpack('>' + 'h' * 0x10, fp.read(0x20))
    
        start_off += 0x20
        decoded = decode_block(fp, start_off, 100000, 100000, coeffs, 0, 0)
        wfp.writeframesraw(''.join(decoded))
