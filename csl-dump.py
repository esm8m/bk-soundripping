#! /usr/bin/env python

"""
Converts a single csl file to an equivalent .wav files
first argument: csl file to convert
second argument (optional): directory to put files in
    If second argument not given, the directory containing the lines in the csl
    will be given the name of the original file and put in the directory of the 
    script. In either case, the individual lines will be named after the
    internal numbering of the lines in the CSL.
"""

import pyximport
pyximport.install()

from adpcm import decode_block

import os.path
import struct
import sys
import wave

class CSL:
    def __init__(self):
        self.parts = []
        self.parts_off = []

    def parse(self, fp):
        fp.seek(0)
        magic = struct.unpack('4s', fp.read(4))[0]
        if magic != b'CSL ':
            raise ValueError("not a valid CSL file (bad magic)")

        nparts = struct.unpack('>I', fp.read(4))[0]
        for i in range(nparts):
            self.parts_off.append(struct.unpack('>I', fp.read(4))[0])

        fp.seek(0, 2)
        self.parts_off.append(fp.tell())

        for (part_start, part_end) in zip(self.parts_off, self.parts_off[1:]):
            fp.seek(part_start)
            self.parts.append(list(fp.read(part_end - part_start)))

        return self

def get_uint32(csf, i):
    return struct.unpack('>I', ''.join(csf[i:i+4]))[0]

def get_song_id(csf):
    return get_uint32(csf, 0x28)

if __name__ == '__main__':
    fp = open(sys.argv[1], 'rb')
    if len(sys.argv) == 3:
        outpath = sys.argv[2]
        #make the directory if it doesn't already exist
        if not os.path.exists(outpath):
            os.makedirs(outpath)
    else:
        outpath = os.path.join('.',os.path.split(sys.argv[1])[1])

    csl = CSL()
    csl.parse(fp)

    print "%d sounds" % len(csl.parts)
    for csf, off in zip(csl.parts, csl.parts_off):
        print "- %08X (at offset %08X)" % (get_song_id(csf), off)

        wfp = wave.open(os.path.join(outpath, "%08X.wav" % get_song_id(csf)),'wb')
        wfp.setnchannels(1)
        wfp.setsampwidth(2)
        wfp.setframerate(22050)
        wfp.setnframes(1000000) # arbitrary... to fix?

        fp.seek(off + 0xA0)
        coeffs = struct.unpack('>hhhhhhhhhhhhhhhh', fp.read(0x20))
        decoded = decode_block(fp, off + 0xC0, 1000000, 1000000,
                               coeffs, 0, 0)
        wfp.writeframesraw(''.join(decoded))
