#! /usr/bin/env python2

import pyximport
pyximport.install()

from adpcm import decode_block

import struct
import sys
import wave

if __name__ == '__main__':
    nsamples = int(sys.argv[2])
    start_off = int(sys.argv[3], 16)

    wfp = wave.open('dump.wav', 'wb')
    wfp.setnchannels(1)
    wfp.setsampwidth(2)
    wfp.setframerate(22050)
    wfp.setnframes(nsamples)

    fp = open(sys.argv[1], 'rb')

    fp.seek(start_off)
    coeffs = struct.unpack('>' + 'h' * 0x10, fp.read(0x20))

    start_off += 0x20
    decoded = decode_block(fp, start_off, nsamples, nsamples, coeffs, 0, 0)
    wfp.writeframesraw(''.join(decoded))
